package ru.jbs.guessnumber;

/**
 * Класс GameLauncher демонстрирует игру угадай число.
 */
public class GameLauncher {
    public static void main(String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}
