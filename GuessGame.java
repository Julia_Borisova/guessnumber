package ru.jbs.guessnumber;

/**
 * В Классе GuessGame угадывается число.
 */
public class GuessGame {

    Player playerOne;
    Player playerToo;
    Player playerThree;

    /**
     * Метод startGame определяет кто из игроков угадал число.
     */

    public void startGame(){
        playerOne = new Player();
        playerToo = new Player();
        playerThree = new Player();
        int guesserOne = 0;
        int guesserToo = 0;
        int guesserThree = 0;
        boolean pOneIsRight = false;
        boolean pTooisRight = false;
        boolean pThreeisRight = false;
        int targetNumber = (int) (Math.random() * 10);
        System.out.println("Я думаю число между 0 и 9..."); //I'm thinking of a number between 0 and 9...
        while (true){
            playerOne.Guess();
            playerToo.Guess();
            playerThree.Guess();

            guesserOne = playerOne.number;
            guesserToo = playerToo.number;
            guesserThree = playerThree.number;

            if (guesserOne == targetNumber) {
                pOneIsRight = true;
            }
            if (guesserToo == targetNumber){
                pTooisRight = true;
            }
            if (guesserThree == targetNumber){
                pThreeisRight = true;
            }

            if (pOneIsRight || pTooisRight || pThreeisRight){
                System.out.println("У нас пободитель! "); // We have a winner!
                System.out.println("Певый игрок прав? " + pOneIsRight); // Player one got it right?
                System.out.println("Второй игрок прав? " + pTooisRight);
                System.out.println("Третй игрок прав? " + pThreeisRight);
                System.out.println("Конец игры"); // Game is over
                break;
            }
            else {
                System.out.println("Игрокам придется повторить попытку."); // Players will have to try again.
            }

        }
    }
}
