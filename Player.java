package ru.jbs.guessnumber;

/**
 * Класс Player предпологает число.
 */
public class Player {
    int number = 0;
    public void Guess(){
        number = (int) (Math.random() * 10);
        System.out.println("Я предполагаю " + number); // I'm guessing
    }
}
